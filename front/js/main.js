// Open-close FAQ
let details = document.querySelectorAll("details");
for(let i = 0; i < details.length; i++) {
    details[i].addEventListener("toggle", accordion);
}
function accordion(event) {
    if (!event.target.open) return;
    let details = event.target.parentNode.children;
    for(let i = 0; i < details.length; i++) {
        if (details[i].tagName !== "DETAILS" ||
            !details[i].hasAttribute('open') ||
            event.target === details[i]) {
            continue;
        }
        details[i].removeAttribute("open");
    }
}

// Tabs
document.addEventListener('DOMContentLoaded', function(e){
    'use strict';
    let list = document.querySelectorAll('#tabNav a');
    list = Array.prototype.slice.call(list, 0);
    list.forEach(function(el, i, ar) {
        el.addEventListener('click', function(event){
            event.preventDefault();
            let tab = document.querySelector(el.getAttribute('href'));

            document.querySelector('#tabNav .active')
                .classList.remove('active');
            document.querySelector('#tabsWrap .active')
                .classList.remove('active');

            el.classList.add('active');
            tab.classList.add('active');
        })
    })
})

let promotionsInfo = document.querySelector('.promotions__info')
let promotionsBtn = document.querySelector('.promotions__more-btn')

promotionsBtn.addEventListener('click', () => {
    promotionsInfo.classList.toggle('toggle-list');
    promotionsBtn.style.display = "none";
});